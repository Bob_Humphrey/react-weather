data = cityData.daily.data[dayNumber]
if (!data) return null
sunriseTime = data[dayNumber].sunriseTime
sunsetTime = data[dayNumber].sunsetTime
sunrise = moment.unix(sunriseTime).format('h:mm')
sunset = moment.unix(sunsetTime).format('h:mm')
moonPhase = Math.round(data.moonPhase * 100)




<Header
componentWidth="w-5/6"
status={displayDate}
city={this.props.city}
showBack={true}
showSelectBox={false}
units={this.props.units}
onClick={this.props.onClick}
/>

